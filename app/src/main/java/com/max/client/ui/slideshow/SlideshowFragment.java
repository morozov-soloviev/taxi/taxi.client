package com.max.client.ui.slideshow;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.max.client.AuthActivity;
import com.max.client.MainActivity;
import com.max.client.R;
import com.max.client.RVAdapter;
import com.max.client.RestService.ClientController;
import com.max.client.RestService.Entry.Client;
import com.max.client.RestService.Entry.ListOrder;
import com.max.client.RestService.Interfaces.GetDataInfoOrder;
import com.max.client.RestService.Interfaces.GetValueDataClient;
import com.max.client.RestService.Interfaces.GetValueURL;
import com.max.client.RestService.RetrofitBuild;

import java.util.ArrayList;

public class SlideshowFragment extends Fragment {

    private SlideshowViewModel slideshowViewModel;
    View root;

    private EditText inpLoginAcc, inpPasswordAcc, inpNameAcc, inpSurnameAcc, inpPhoneAcc, inpEmailAcc;
    private Button update, logout;
    private ClientController client;
    private ScrollView scr;
    private ProgressBar progress;
    Client cl;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        slideshowViewModel =
                ViewModelProviders.of(this).get(SlideshowViewModel.class);
        root = inflater.inflate(R.layout.account_page, container, false);

        inpLoginAcc = (EditText) root.findViewById(R.id.input_login_acc);
        inpLoginAcc.setShowSoftInputOnFocus(false);
        inpPasswordAcc = (EditText) root.findViewById(R.id.input_password_acc);
        inpNameAcc = (EditText) root.findViewById(R.id.input_name_acc);
        inpSurnameAcc = (EditText) root.findViewById(R.id.input_surname_acc);
        inpPhoneAcc = (EditText) root.findViewById(R.id.input_phone_acc);
        inpEmailAcc = (EditText) root.findViewById(R.id.input_email_acc);

        progress= (ProgressBar)root.findViewById(R.id.progressLoadAcc);
        scr= (ScrollView) root.findViewById(R.id.scr);
        update = (Button) root.findViewById(R.id.btnUpdate);
        logout = (Button) root.findViewById(R.id.btnOut);

        client = new ClientController();

        client.info(new GetValueDataClient() {
            @Override
            public void onSuccessInfo(@NonNull JsonObject json) {

                inpLoginAcc.setText(MainActivity.Login);
                inpPasswordAcc.setText(MainActivity.Password);
                inpNameAcc.setText(json.getAsJsonObject("client").get("firstName").getAsString());
                inpSurnameAcc.setText(json.getAsJsonObject("client").get("lastName").getAsString());
                inpPhoneAcc.setText(json.getAsJsonObject("client").get("phone").getAsString());
                inpEmailAcc.setText(json.getAsJsonObject("client").get("email").getAsString());
            }

            @Override
            public void onErrorInfo(@NonNull Throwable throwable) {

            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                cl = new Client();
                cl.setPassword(inpPasswordAcc.getText().toString());
                cl.setFirstName(inpNameAcc.getText().toString());
                cl.setLastName(inpSurnameAcc.getText().toString());
                cl.setEmail(inpEmailAcc.getText().toString());
                cl.setPhone(inpPhoneAcc.getText().toString());

                AlertDialog.Builder adBuilder = new AlertDialog.Builder(root.getContext());
                adBuilder.setTitle("Обновить данные?")
                        .setCancelable(false);

                adBuilder.setNegativeButton("Назад", null);

                adBuilder.setPositiveButton("Да",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog,
                                                int id) {
                                progress.setVisibility(View.VISIBLE);
                                scr.setVisibility(View.INVISIBLE);

                                client.update(new GetValueURL() {
                                    @Override
                                    public void onSuccessLogin(@NonNull String token) {

                                    }

                                    @Override
                                    public void onErrorLogin(@NonNull String s) {

                                    }

                                    @Override
                                    public void onSuccessRegister(@NonNull boolean success) {
                                        if (success) {
                                            progress.setVisibility(View.INVISIBLE);
                                            scr.setVisibility(View.VISIBLE);
                                            Toast.makeText(root.getContext(), "Данные обновлены!", Toast.LENGTH_SHORT).show();

                                        }
                                    }

                                    @Override
                                    public void onErrorRegister(@NonNull String s) {

                                    }
                                }, cl);

                            }
                        });

                AlertDialog alert = adBuilder.create();
                alert.show();
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder adBuilder = new AlertDialog.Builder(root.getContext());
                adBuilder.setTitle("Вы уверены, что хотите выйти?")
                        .setCancelable(false);

                adBuilder.setNegativeButton("Отмена", null);

                adBuilder.setPositiveButton("Да",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog,
                                                int id) {
                                RetrofitBuild.token = null;
                                Intent intent = new Intent(root.getContext(), AuthActivity.class);
                                startActivity(intent);

                            }
                        });

                AlertDialog alert = adBuilder.create();
                alert.show();
            }
        });

        return root;
    }
}

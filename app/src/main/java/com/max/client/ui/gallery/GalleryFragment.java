package com.max.client.ui.gallery;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.max.client.R;
import com.max.client.RVAdapter;
import com.max.client.RestService.ClientController;
import com.max.client.RestService.Entry.ListOrder;
import com.max.client.RestService.Interfaces.GetDataInfoOrder;
import com.yandex.mapkit.geometry.Point;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class GalleryFragment extends Fragment {

    private GalleryViewModel galleryViewModel;

    private View root;
    private RecyclerView rvListOrder;
    private LinearLayoutManager llm;
    private ClientController client;
    private ProgressBar progressListOrder;
    private ArrayList<ListOrder> listOrder;
    RVAdapter rvAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        galleryViewModel =
                ViewModelProviders.of(this).get(GalleryViewModel.class);
        root = inflater.inflate(R.layout.history_order, container, false);

        client = new ClientController();
        rvListOrder = (RecyclerView) root.findViewById(R.id.rv);
        progressListOrder = (ProgressBar) root.findViewById(R.id.progressOrder);
        llm = new LinearLayoutManager(root.getContext());
        rvListOrder.setLayoutManager(llm);
        GetListOrderAv();

        return root;
    }

    String[] dateOrder;

    public void GetListOrderAv() {

        client.getListOrder(new GetDataInfoOrder() {
            @Override
            public void onSuccessInfoOrder(@NonNull JsonObject json) {

                progressListOrder.setVisibility(View.INVISIBLE);
                listOrder = new ArrayList<>();
                ArrayList<ListOrder> sortListOrder = new ArrayList<ListOrder>();
                JsonArray orders = json.getAsJsonArray("orders");
                dateOrder = new String[orders.size()];
                ArrayList<Integer> listNum = new ArrayList<Integer>();

                for (int i = 0; i < orders.size(); i++) {
                    listNum.add(i);
                    sortListOrder.add(new ListOrder(orders.get(i).getAsJsonObject()));
                }

                DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                for(int k =0;k<orders.size();k++) {
                    for (int i = 0; i < orders.size() - 1; i++) {
                        Date date1 = new Date();
                        Date date2 = new Date();
                        try {
                            date1 = format.parse(sortListOrder.get(i).getDataStart());
                            date2 = format.parse(sortListOrder.get(i + 1).getDataStart());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        if (date1.compareTo(date2) < 0) {
                            ListOrder ao = sortListOrder.get(i);
                            sortListOrder.set(i, sortListOrder.get(i + 1));
                            sortListOrder.set(i + 1, ao);
                            int buf = listNum.get(i);
                            listNum.set(i, listNum.get(i + 1));
                            listNum.set(i + 1, buf);
                        }
                    }
                }
                for (int i = 0; i < orders.size(); i++) {
                    JsonArray routes = orders.get(listNum.get(i)).getAsJsonObject().getAsJsonArray("route");
                    double FromLatitude = routes.get(0).getAsJsonObject().get("latitude").getAsDouble();
                    double FromLongitude = routes.get(0).getAsJsonObject().get("longitude").getAsDouble();

                    double WhereLatitude = routes.get(1).getAsJsonObject().get("latitude").getAsDouble();
                    double WhereLongitude = routes.get(1).getAsJsonObject().get("longitude").getAsDouble();

                    String from = getAdressOnCoords(new Point(FromLatitude, FromLongitude));
                    String where = getAdressOnCoords(new Point(WhereLatitude, WhereLongitude));
                    sortListOrder.get(i).setFrom(from);
                    sortListOrder.get(i).setWhere(where);


                    String[] dataStart = sortListOrder.get(i).getDataStart().split("T");
                    String[] d1 = dataStart[0].split("-");
                    sortListOrder.get(i).setDataStart(d1[0]+"."+d1[1]+"."+d1[2]+ " - " + dataStart[1]);

                    if(sortListOrder.get(i).getDataEnd()!= null) {
                        String[] dataEnd = sortListOrder.get(i).getDataEnd().split("T");
                        String[] d2 = dataEnd[0].split("-");

                        String[] d3 = dataEnd[1].split(":");
                        int y = Integer.parseInt(d3[0]);
                        y = y+4;
                        String[] d4 = d3[2].split(".");
                        if(d4.length!= 0)
                        {
                            String d5 = d4[0];
                            sortListOrder.get(i).setDataEnd(d1[0] + "." + d1[1] + "." + d1[2] + " - " + y + ":" + d3[1] + ":" + d5);
                        }
                        else
                            sortListOrder.get(i).setDataEnd(d1[0] + "." + d1[1] + "." + d1[2] + " - " + y + ":" + d3[1] + ":" + d3[2]);
                    }

                    listOrder.add(sortListOrder.get(i));
                }
                rvAdapter = new RVAdapter(listOrder);
                rvListOrder.setAdapter(rvAdapter);
            }

            @Override
            public void onErrorInfoOrder(@NonNull Throwable throwable) {

            }
        });
    }

    Geocoder geocoder;

    public String getAdressOnCoords(Point point) {

        String ad = "";
        geocoder = new Geocoder(root.getContext(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(point.getLatitude(), point.getLongitude(), 1);

            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                String[] adressPart = returnedAddress.getAddressLine(0).toString().split(",");
                for (int i = 0; i < adressPart.length - 3; i++)
                    ad = ad + adressPart[i] + ", ";
                ad = ad + returnedAddress.getCountryName();

            } else {
                ad = "Error";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ad;
    }
}
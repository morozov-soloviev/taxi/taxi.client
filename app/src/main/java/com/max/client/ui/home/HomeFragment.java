package com.max.client.ui.home;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.max.client.MainActivity;
import com.max.client.R;
import com.max.client.RestService.ClientController;
import com.max.client.RestService.Entry.Calculate;
import com.max.client.RestService.Entry.InfoClient;
import com.max.client.RestService.Entry.Order;
import com.max.client.RestService.Entry.OrderId;
import com.max.client.RestService.Entry.OrderInfo;
import com.max.client.RestService.Entry.Routes;
import com.max.client.RestService.Interfaces.CancelInterface;
import com.max.client.RestService.Interfaces.GetDataInfoOrder;
import com.max.client.RestService.Interfaces.GetOrderData;
import com.max.client.RestService.Interfaces.GetValueDataClient;
import com.yandex.mapkit.Animation;
import com.yandex.mapkit.MapKitFactory;
import com.yandex.mapkit.RequestPoint;
import com.yandex.mapkit.RequestPointType;
import com.yandex.mapkit.directions.DirectionsFactory;
import com.yandex.mapkit.directions.driving.DrivingOptions;
import com.yandex.mapkit.directions.driving.DrivingRoute;
import com.yandex.mapkit.directions.driving.DrivingRouter;
import com.yandex.mapkit.directions.driving.DrivingSession;
import com.yandex.mapkit.geometry.Point;
import com.yandex.mapkit.map.CameraPosition;
import com.yandex.mapkit.map.InputListener;
import com.yandex.mapkit.map.Map;
import com.yandex.mapkit.map.MapObjectCollection;
import com.yandex.mapkit.map.PlacemarkMapObject;
import com.yandex.mapkit.map.PolylineMapObject;
import com.yandex.mapkit.mapview.MapView;
import com.yandex.runtime.Error;
import com.yandex.runtime.image.ImageProvider;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class HomeFragment extends Fragment implements DrivingSession.DrivingRouteListener {

    private HomeViewModel homeViewModel;
    private String ApiKey = "6cec9a45-a6a1-48d3-8884-340f9a2c0fb0";
    private String[] tarifs = {"16e362be-73fc-4319-bcba-8b423c05792a", "a5f9d4ec-0a14-4f16-95a4-7f89da5bed68", "764cb89f-e8e6-4174-85be-c6e7425f6a4f", "7d929aff-0781-4b95-9590-a0c4dcb98fe5"};
    private String[] carTypes = {"0281c710-6e53-4d49-a219-73a27a8d3d5f", "1bafafbd-5ec7-4dbb-8d7d-a1b1b43ba866", "a245f9a1-fa31-4552-bf88-4e4689a0b861", "c3862077-3574-4142-98c9-0aab05c0dd0a"};
    private String[] carOptions = {"b98ab410-bdce-4aea-9300-48f41371873c", "c2d045b7-483d-49fc-9133-f5f8fe5058ca", "4166cfc2-46b2-44a3-9fa2-c21b4d0f12d8"};

    private String[] chooseCarTypes, chooseCarOptions;

    private MapView mapview;
    View root;
    private LocationManager locationManager;
    private Point currentPosition;
    private Geocoder geocoder;
    private MapObjectCollection pointCollection;
    private PlacemarkMapObject placemarkFrom, placemarkWhere, placemarkDriver;
    private Point startPoint, endPoint;
    private DrivingRouter drivingRouter;
    private DrivingSession drivingSession;
    private PolylineMapObject routeLine;

    private Point posDriver;

    private Bitmap fromMark;
    private ImageProvider imageMarkFrom;

    private Bitmap whereMark;
    private ImageProvider imageMarkWhere;

    private Bitmap driverMark;
    private ImageProvider imageMarkDriver;

    private EditText inputFromWay, inputWhereWay;
    private Button readyBtn, next1, next2, createOrder;
    private Spinner spinnerTarifs;
    private ScrollView scrollOrder;
    private TextView price;
    private CheckBox checkSedan, checkHatchback, checkUniver, checkFurgon, checkChild, checkAnimal, checkDelivery;
    private String state;
    private double priceValue;
    private ScrollView orderScroll2;

    private InfoClient infoClient;
    private ClientController client;
    public static String order_id;

    private TextView statusTxt, phoneTxt, CarTxt, priceOrderCheck, fromTextOrder, whereTextOrder;
    private Button btnCancelOrder;
    private ProgressBar progress;

    private int chooseTarif = 0;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        root = inflater.inflate(R.layout.taxi_order, container, false);

        requestPermission();


        mapview = (MapView) root.findViewById(R.id.mapview);

        orderScroll2 = (ScrollView) root.findViewById(R.id.scrollOrder2);

        statusTxt = (TextView) root.findViewById(R.id.status);
        phoneTxt = (TextView) root.findViewById(R.id.phone);
        CarTxt = (TextView) root.findViewById(R.id.carTxt);
        priceOrderCheck = (TextView) root.findViewById(R.id.priceOrder);
        btnCancelOrder = (Button) root.findViewById(R.id.btnCancelOrder);
        fromTextOrder = (TextView) root.findViewById(R.id.fromOrderFinal);
        whereTextOrder = (TextView) root.findViewById(R.id.whereOrderFinal);
        progress = (ProgressBar) root.findViewById(R.id.progressBar2);

        fromMark = BitmapFactory.decodeResource(getResources(),
                R.drawable.icon_client_point);
        fromMark = Bitmap.createScaledBitmap(fromMark, 75, 75, true);

        imageMarkFrom = new ImageProvider() {
            @Override
            public String getId() {
                return "imageMarkFrom";
            }

            @Override
            public Bitmap getImage() {
                return fromMark;
            }
        };

        driverMark = BitmapFactory.decodeResource(getResources(),
                R.drawable.driver_mark);
        driverMark = Bitmap.createScaledBitmap(driverMark, 75, 75, true);

        imageMarkDriver = new ImageProvider() {
            @Override
            public String getId() {
                return "imageMarkDriver";
            }

            @Override
            public Bitmap getImage() {
                return driverMark;
            }
        };

        whereMark = BitmapFactory.decodeResource(getResources(),
                R.drawable.mark);
        whereMark = Bitmap.createScaledBitmap(whereMark, 60, 134, true);

        imageMarkWhere = new ImageProvider() {
            @Override
            public String getId() {
                return "imageMarkWhere";
            }

            @Override
            public Bitmap getImage() {
                return whereMark;
            }
        };

        inputFromWay = (EditText) root.findViewById(R.id.inputFromWay);
        inputFromWay.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    state = "from";
                }
            }
        });

        inputWhereWay = (EditText) root.findViewById(R.id.inputWhereWay);
        inputWhereWay.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    state = "where";
                }
            }
        });

        readyBtn = (Button) root.findViewById(R.id.ready);
        readyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (startPoint != null && endPoint != null) {
                    drivingRouter = DirectionsFactory.getInstance().createDrivingRouter();
                    submitRequest(startPoint, endPoint);


                    scrollOrder.scrollTo(0, 600);
                    GetCalculatePrice();
                } else {
                    Toast to = Toast.makeText(root.getContext(), "Заполните поля", Toast.LENGTH_SHORT);

                    View v1 = to.getView();
                    v1.setAlpha(0.92f);
                    v1.setBackgroundColor(getResources().getColor(R.color.colorMainBtn));
                    TextView t0 = v1.findViewById(android.R.id.message);
                    t0.setTextColor(Color.rgb(56, 56, 56));
                    t0.setTextSize(16);
                    t0.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    t0.setPadding(13, 13, 13, 13);
                    to.setGravity(Gravity.TOP, 0, 200);
                    to.show();
                }
            }
        });

        btnCancelOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        inputFromWay.setShowSoftInputOnFocus(false);
        inputWhereWay.setShowSoftInputOnFocus(false);

        spinnerTarifs = (Spinner) root.findViewById(R.id.spinnerTarifs);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(root.getContext(), R.array.tarifsName, R.layout.spinner_item);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerTarifs.setAdapter(adapter);
        spinnerTarifs.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                chooseTarif = position;
                if (startPoint != null && endPoint != null)
                    GetCalculatePrice();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        scrollOrder = (ScrollView) root.findViewById(R.id.scrollOrder);
        price = (TextView) root.findViewById(R.id.price);

        checkSedan = (CheckBox) root.findViewById(R.id.checkSedan);
        checkHatchback = (CheckBox) root.findViewById(R.id.checkHatchback);
        checkUniver = (CheckBox) root.findViewById(R.id.checkUniver);
        checkFurgon = (CheckBox) root.findViewById(R.id.checkFurgon);
        checkChild = (CheckBox) root.findViewById(R.id.checkChild);
        checkAnimal = (CheckBox) root.findViewById(R.id.checkAnimal);
        checkDelivery = (CheckBox) root.findViewById(R.id.checkDelivery);

        next1 = (Button) root.findViewById(R.id.next1);
        next1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (startPoint != null && endPoint != null) {
                    scrollOrder.scrollTo(600, 1248);

                } else {
                }
            }
        });

        next2 = (Button) root.findViewById(R.id.next2);
        next2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (startPoint != null && endPoint != null) {
                    scrollOrder.scrollTo(1248, 1945);
                    SetChooseCarTypes();

                } else {
                }
            }
        });

        createOrder = (Button) root.findViewById(R.id.createOrder);
        createOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (startPoint != null && endPoint != null) {
                    scrollOrder.setVisibility(View.INVISIBLE);
                    orderScroll2.setVisibility(View.VISIBLE);
                    SetChooseCarTypes();
                    SetChooseCarOptions();
                    OrderSend();
                } else {
                }
            }
        });


        pointCollection = mapview.getMap().getMapObjects().addCollection();

        locationManager = (LocationManager) this.getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            getLocation();
        }

        if (currentPosition != null) {
            placemarkFrom = pointCollection.addPlacemark(currentPosition, imageMarkFrom);
            inputFromWay.setText(getAdressOnCoords(currentPosition));
            fromTextOrder.setText(getAdressOnCoords(currentPosition));
            startPoint = currentPosition;
            state = "where";
        }

        InputListener inputListener = new InputListener() {
            @Override
            public void onMapTap(@NonNull Map map, @NonNull Point point) {

            }

            @Override
            public void onMapLongTap(@NonNull Map map, @NonNull Point point) {

                if (state == "from") {
                    if (placemarkFrom == null)
                        placemarkFrom = pointCollection.addPlacemark(point, imageMarkFrom);
                    else
                        placemarkFrom.setGeometry(point);
                    inputFromWay.setText(getAdressOnCoords(point));
                    fromTextOrder.setText(getAdressOnCoords(point));
                    startPoint = point;
                } else {
                    if (placemarkWhere == null)
                        placemarkWhere = pointCollection.addPlacemark(point, imageMarkWhere);
                    else
                        placemarkWhere.setGeometry(point);
                    inputWhereWay.setText(getAdressOnCoords(point));
                    whereTextOrder.setText(getAdressOnCoords(point));
                    endPoint = point;
                }

            }
        };
        mapview.getMap().addInputListener(inputListener);

        btnCancelOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                client.cancelOrder(new CancelInterface() {
                    @Override
                    public void onSuccessCancel(@NonNull boolean success) {

                        statePosUpdate = false;
                        scrollOrder.setVisibility(View.VISIBLE);
                        orderScroll2.setVisibility(View.INVISIBLE);
                        order_id = "";
                        getLocation();
                        inputWhereWay.setText("");
                        price.setText("");
                        scrollOrder.scrollTo(scrollOrder.getScrollX(), 0);
                        endPoint = null;
                        Toast toast = Toast.makeText(root.getContext(), "Заказ отменен!", Toast.LENGTH_SHORT);
                        View vvv = toast.getView();
                        vvv.setAlpha(0.92f);
                        vvv.setBackgroundColor(getResources().getColor(R.color.colorMainBtn));
                        TextView t1 = vvv.findViewById(android.R.id.message);
                        t1.setTextColor(Color.rgb(56, 56, 56));
                        t1.setTextSize(16);
                        t1.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                        t1.setPadding(13, 13, 13, 13);
                        toast.setGravity(Gravity.TOP, 0, 200);
                        toast.show();

                    }

                    @Override
                    public void onErrorCancel(@NonNull Throwable throwable) {

                    }
                }, new OrderId(order_id));
            }
        });

        return root;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MapKitFactory.setApiKey(ApiKey);
        MapKitFactory.initialize(this.getContext());
        DirectionsFactory.initialize(this.getContext());
        client = new ClientController();

        client.info(new GetValueDataClient() {
            @Override
            public void onSuccessInfo(@NonNull JsonObject json) {

                if (json.has("currentOrder")) {
                    drivingRouter = DirectionsFactory.getInstance().createDrivingRouter();
                    order_id = json.getAsJsonObject("currentOrder").get("id").getAsString();
                    scrollOrder.setVisibility(View.INVISIBLE);
                    orderScroll2.setVisibility(View.VISIBLE);

                    JsonArray jarr = json.getAsJsonObject("currentOrder").getAsJsonArray("route");
                    startPoint = new Point(jarr.get(0).getAsJsonObject().get("latitude").getAsDouble(), jarr.get(0).getAsJsonObject().get("longitude").getAsDouble());
                    endPoint = new Point(jarr.get(1).getAsJsonObject().get("latitude").getAsDouble(), jarr.get(1).getAsJsonObject().get("longitude").getAsDouble());

                    if (placemarkFrom == null)
                        placemarkFrom = pointCollection.addPlacemark(startPoint, imageMarkFrom);
                    else
                        placemarkFrom.setGeometry(startPoint);
                    statePosUpdate = true;
                    updateStatus();
                }

            }

            @Override
            public void onErrorInfo(@NonNull Throwable throwable) {

            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        mapview.onStart();
        MapKitFactory.getInstance().onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapview.onStop();
        MapKitFactory.getInstance().onStop();
    }

    public void moveCameraToPosition(@NonNull Point target) {
        mapview.getMap().move(
                new CameraPosition(target, 15.0f, 0.0f, 0.0f),
                new Animation(Animation.Type.SMOOTH, 2), null);
    }

    @Override
    public void onDrivingRoutes(List<DrivingRoute> routes) {
        DrivingRoute route = routes.get(0);
        if (routeLine == null) {
            routeLine = pointCollection.addPolyline(route.getGeometry());
        } else
            routeLine.setGeometry(route.getGeometry());

    }

    @Override
    public void onDrivingRoutesError(Error error) {
        String errorMessage = error.toString();
        Toast.makeText(this.getContext(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    private void submitRequest(Point start, Point end) {
        DrivingOptions options = new DrivingOptions();
        ArrayList<RequestPoint> requestPoints = new ArrayList<>();
        requestPoints.add(new RequestPoint(start, RequestPointType.WAYPOINT, null));
        requestPoints.add(new RequestPoint(end, RequestPointType.WAYPOINT, null));
        drivingSession = drivingRouter.requestRoutes(requestPoints, options, this);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this.getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
    }

    public String getAdressOnCoords(Point point) {

        String ad = "";
        geocoder = new Geocoder(root.getContext(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(point.getLatitude(), point.getLongitude(), 1);

            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                String[] adressPart = returnedAddress.getAddressLine(0).toString().split(",");
                for (int i = 0; i < adressPart.length - 3; i++)
                    ad = ad + adressPart[i] + ", ";
                ad = ad + returnedAddress.getCountryName();

            } else {
                ad = "Error";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ad;
    }

    private void getLocation() {

        if (ActivityCompat.checkSelfPermission(root.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(root.getContext(),

                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this.getActivity(), new String[]
                    {Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            Location LocationGps = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location LocationNetwork = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Location LocationPassive = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

            if (LocationPassive != null) {
                currentPosition = new Point(LocationPassive.getLatitude(), LocationPassive.getLongitude());
                inputFromWay.setText(getAdressOnCoords(currentPosition));
            } else if (LocationNetwork != null) {
                currentPosition = new Point(LocationNetwork.getLatitude(), LocationNetwork.getLongitude());
                inputFromWay.setText(getAdressOnCoords(currentPosition));
            } else if (LocationGps != null) {
                currentPosition = new Point(LocationGps.getLatitude(), LocationGps.getLongitude());
                inputFromWay.setText(getAdressOnCoords(currentPosition));
            } else {
                Toast.makeText(root.getContext(), "Can't Get Your Location", Toast.LENGTH_SHORT).show();
            }
            moveCameraToPosition(currentPosition);

        }
    }

    public void GetCalculatePrice() {
        Calculate calculate = new Calculate();
        calculate.setTarifId(tarifs[chooseTarif]);
        Routes[] routes = new Routes[2];
        routes[0] = new Routes();
        routes[1] = new Routes();
        routes[0].setLatitude(startPoint.getLatitude());
        routes[0].setLongitude(startPoint.getLongitude());
        routes[1].setLatitude(endPoint.getLatitude());
        routes[1].setLongitude(endPoint.getLongitude());
        calculate.setRoute(routes);

        client.calculate(calculate, new GetOrderData() {
            @Override
            public void onSuccessCalculate(@NonNull double amount) {
                priceValue = amount;
                price.setText(priceValue + "р.");
                priceOrderCheck.setText(priceValue + "р.");
            }

            @Override
            public void onErrorCalculate(@NonNull Throwable throwable) {

            }

            @Override
            public void onSuccessCreateOrder(@NonNull String orderId) {

            }

            @Override
            public void onErrorCreateOrder(@NonNull Throwable throwable) {

            }
        });
    }

    public void SetChooseCarTypes() {
        ArrayList<Integer> list = new ArrayList<>();
        if (checkSedan.isChecked())
            list.add(0);
        if (checkUniver.isChecked())
            list.add(1);
        if (checkHatchback.isChecked())
            list.add(2);
        if (checkFurgon.isChecked())
            list.add(3);

        chooseCarTypes = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            chooseCarTypes[i] = carTypes[list.get(i)];
        }
    }

    public void SetChooseCarOptions() {
        ArrayList<Integer> list = new ArrayList<>();
        if (checkChild.isChecked())
            list.add(0);
        if (checkAnimal.isChecked())
            list.add(1);
        if (checkDelivery.isChecked())
            list.add(2);

        chooseCarOptions = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            chooseCarOptions[i] = carOptions[list.get(i)];
        }
    }

    public void OrderSend() {
        Order order = new Order();
        order.setTarifId(tarifs[chooseTarif]);
        Routes[] routes = new Routes[2];
        routes[0] = new Routes();
        routes[1] = new Routes();
        routes[0].setLatitude(startPoint.getLatitude());
        routes[0].setLongitude(startPoint.getLongitude());
        routes[1].setLatitude(endPoint.getLatitude());
        routes[1].setLongitude(endPoint.getLongitude());
        order.setRoute(routes);

        order.setCarTypesIds(chooseCarTypes);
        order.setCarOptionsIds(chooseCarOptions);

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String date = df.format(Calendar.getInstance().getTime());
        order.setStart(date);

        client.createOrder(order, new GetOrderData() {
            @Override
            public void onSuccessCalculate(@NonNull double amount) {

            }

            @Override
            public void onErrorCalculate(@NonNull Throwable throwable) {

            }

            @Override
            public void onSuccessCreateOrder(@NonNull String orderId) {
                order_id = orderId;
                Toast toast = Toast.makeText(root.getContext(), "Заказ успешно добавлен", Toast.LENGTH_SHORT);
                View v = toast.getView();
                v.setAlpha(0.92f);
                v.setBackgroundColor(getResources().getColor(R.color.colorMainBtn));
                TextView t2 = v.findViewById(android.R.id.message);
                t2.setTextColor(Color.rgb(56, 56, 56));
                t2.setTextSize(16);
                t2.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                t2.setPadding(13, 13, 13, 13);
                toast.setGravity(Gravity.TOP, 0, 200);
                toast.show();
                statePosUpdate = true;

                if (placemarkWhere != null) {
                    pointCollection.remove(placemarkWhere);
                    placemarkWhere = null;
                }
                progress.setVisibility(View.VISIBLE);
                if (routeLine != null) {
                    pointCollection.remove(routeLine);
                    routeLine = null;
                }
                updateStatus();
            }

            @Override
            public void onErrorCreateOrder(@NonNull Throwable throwable) {

            }
        });
    }

    void upd() {
        if (statePosUpdate)
            updateStatus();
    }

    boolean statePosUpdate = false;

    boolean stateSearch = true;
    String statusValue = "";

    public void updateStatus() {
        if (startPoint != null) {

            client.infoOrder(new GetDataInfoOrder() {
                @Override
                public void onSuccessInfoOrder(@NonNull JsonObject json) {

                    String status = json.get("status").getAsString();


                    if (status.equals("Created")) {
                        if (!statusValue.equals(status)) {
                            statusValue = status;
                            statusTxt.setText("Поиск водителя");
                        }
                        upd();

                    } else if (status.equals("OnWayToClient") || status.equals("Waiting")) {

                        Point curPos = posDriver;
                        if(curPos==null)
                            curPos = new Point(0.0,0.0);
                        posDriver = new Point(json.getAsJsonObject("board").get("latitude").getAsDouble(), json.getAsJsonObject("board").get("longitude").getAsDouble());

                        if (stateSearch) {
                            progress.setVisibility(View.INVISIBLE);
                            stateSearch = false;
                            OrderInfo orderInfo = new OrderInfo(json);
                            phoneTxt.setText(orderInfo.getPhoneDriver());
                            CarTxt.setText(orderInfo.getMark() + " " + orderInfo.getModel() + " - " + orderInfo.getNumber());
                            fromTextOrder.setText(getAdressOnCoords(new Point(orderInfo.getLatitudeFrom(), orderInfo.getLongitudeFrom())));
                            whereTextOrder.setText(getAdressOnCoords(new Point(orderInfo.getLatitudeWhere(), orderInfo.getLongitudeWhere())));
                            priceOrderCheck.setText(orderInfo.getAmount() + " р.");
                        }

                        if (status.equals("OnWayToClient")) {
                            if (!statusValue.equals(status)) {
                                statusValue = status;
                                statusTxt.setText("Водитель едет к вам");
                                submitRequest(startPoint, posDriver);
                            }

                            if (placemarkDriver == null)
                                placemarkDriver = pointCollection.addPlacemark(posDriver, imageMarkDriver);
                            else
                                placemarkDriver.setGeometry(posDriver);

                            if ((curPos.getLatitude() != posDriver.getLatitude())|| (curPos.getLongitude() != posDriver.getLongitude()))
                                submitRequest(startPoint, posDriver);

                        } else if (status.equals("Waiting")) {

                            if (!statusValue.equals(status)) {
                                statusValue = status;
                                statusTxt.setText("Водитель вас ожидает");
                            }
                            posDriver = new Point(json.getAsJsonObject("board").get("latitude").getAsDouble(), json.getAsJsonObject("board").get("longitude").getAsDouble());
                            if (placemarkDriver == null)
                                placemarkDriver = pointCollection.addPlacemark(posDriver, imageMarkDriver);
                            else
                                placemarkDriver.setGeometry(posDriver);
                        }
                        upd();

                    } else if (status.equals("OnWay")) {

                        Point curPos = posDriver;
                        if(curPos==null)
                            curPos = new Point(0.0,0.0);
                        posDriver = new Point(json.getAsJsonObject("board").get("latitude").getAsDouble(), json.getAsJsonObject("board").get("longitude").getAsDouble());

                        if (!statusValue.equals(status)) {
                            statusValue = status;
                            submitRequest(posDriver, endPoint);
                            btnCancelOrder.setEnabled(false);
                            statusTxt.setText("В пути");
                        }

                        if (stateSearch) {
                            progress.setVisibility(View.INVISIBLE);
                            stateSearch = false;
                            OrderInfo orderInfo = new OrderInfo(json);
                            phoneTxt.setText(orderInfo.getPhoneDriver());
                            CarTxt.setText(orderInfo.getMark() + " " + orderInfo.getModel() + " - " + orderInfo.getNumber());
                            fromTextOrder.setText(getAdressOnCoords(new Point(orderInfo.getLatitudeFrom(), orderInfo.getLongitudeFrom())));
                            whereTextOrder.setText(getAdressOnCoords(new Point(orderInfo.getLatitudeWhere(), orderInfo.getLongitudeWhere())));
                            priceOrderCheck.setText(orderInfo.getAmount() + " р.");
                        }

                        if (placemarkWhere == null)
                            placemarkWhere = pointCollection.addPlacemark(endPoint, imageMarkWhere);

                        if (placemarkFrom != null) {
                            pointCollection.remove(placemarkFrom);
                            placemarkFrom = null;
                        }

                        if (placemarkDriver == null)
                            placemarkDriver = pointCollection.addPlacemark(posDriver, imageMarkDriver);
                        else
                            placemarkDriver.setGeometry(posDriver);

                        if (statePosUpdate) {
                            if ((curPos.getLatitude() != posDriver.getLatitude()) || (curPos.getLongitude() != posDriver.getLongitude()))
                                submitRequest(posDriver, endPoint);
                        }
                        upd();

                    } else if (status.equals("Completed")) {

                        scrollOrder.setVisibility(View.VISIBLE);
                        orderScroll2.setVisibility(View.INVISIBLE);

                        order_id = "";

                        getLocation();

                        inputWhereWay.setText("");
                        price.setText("");
                        scrollOrder.scrollTo(scrollOrder.getScrollX(), 0);

                        if (placemarkFrom == null)
                            placemarkFrom = pointCollection.addPlacemark(currentPosition, imageMarkFrom);
                        else
                            placemarkFrom.setGeometry(currentPosition);

                        if (routeLine != null) {
                            pointCollection.remove(routeLine);
                            routeLine = null;
                        }
                        if (placemarkDriver != null) {
                            pointCollection.remove(placemarkDriver);
                            placemarkDriver = null;
                        }
                        if (placemarkWhere != null) {
                            pointCollection.remove(placemarkWhere);
                            placemarkWhere = null;
                        }
                        endPoint = null;
                        posDriver = null;
                        Toast toast = Toast.makeText(root.getContext(), "Вы прибыли в точку назначения!", Toast.LENGTH_SHORT);
                        View v2 = toast.getView();
                        v2.setAlpha(0.92f);
                        v2.setBackgroundColor(getResources().getColor(R.color.colorMainBtn));
                        TextView t3 = v2.findViewById(android.R.id.message);
                        t3.setTextColor(Color.rgb(56, 56, 56));
                        t3.setTextSize(16);
                        t3.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                        t3.setPadding(13, 13, 13, 13);
                        toast.setGravity(Gravity.TOP, 0, 200);
                        toast.show();

                        statePosUpdate = false;

                    } else if (status.equals("Canceled")) {

                        scrollOrder.setVisibility(View.VISIBLE);
                        orderScroll2.setVisibility(View.INVISIBLE);

                        order_id = "";

                        getLocation();


                        if (placemarkFrom == null)
                            placemarkFrom = pointCollection.addPlacemark(currentPosition, imageMarkFrom);
                        else
                            placemarkFrom.setGeometry(currentPosition);

                        if (routeLine != null) {
                            pointCollection.remove(routeLine);
                            routeLine = null;
                        }
                        if (placemarkDriver != null) {
                            pointCollection.remove(placemarkDriver);
                            placemarkDriver = null;
                        }
                        if (placemarkWhere != null) {
                            pointCollection.remove(placemarkWhere);
                            placemarkWhere = null;
                        }

                        inputWhereWay.setText("");
                        price.setText("");
                        scrollOrder.scrollTo(scrollOrder.getScrollX(), 0);
                        endPoint = null;
                        posDriver = null;
                        Toast ttt = Toast.makeText(root.getContext(), "ЗАКАЗ БЫЛ ОТМЕНЕН ВОДИТЕЛЕМ!", Toast.LENGTH_SHORT);
                        View v3 = ttt.getView();
                        v3.setBackgroundColor(getResources().getColor(R.color.colorMainBtn));
                        v3.setAlpha(0.92f);
                        TextView t4 = v3.findViewById(android.R.id.message);
                        t4.setTextColor(Color.rgb(56, 56, 56));
                        t4.setTextSize(16);
                        t4.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                        t4.setPadding(13, 13, 13, 13);
                        ttt.setGravity(Gravity.TOP, 0, 200);
                        if(statePosUpdate)
                        ttt.show();
                        statePosUpdate = false;
                    }

                }

                @Override
                public void onErrorInfoOrder(@NonNull Throwable throwable) {
                    Log.d("ERRRRRRRRRRR", throwable.getMessage());
                }
            }, new OrderId(order_id));
        }
    }
}
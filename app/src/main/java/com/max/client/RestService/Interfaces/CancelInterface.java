package com.max.client.RestService.Interfaces;

import androidx.annotation.NonNull;

public interface CancelInterface {

    void onSuccessCancel(@NonNull boolean success);
    void onErrorCancel(@NonNull Throwable throwable);
}

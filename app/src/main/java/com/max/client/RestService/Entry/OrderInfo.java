package com.max.client.RestService.Entry;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class OrderInfo {

    private String mark;
    private String model;
    private String number;
    private String phoneDriver;
    private String from, where;
    private double latitudeFrom, longitudeFrom;
    private double latitudeWhere, longitudeWhere;
    private double latitude, longitude;
    private double amount;
    private String status;

    public OrderInfo(JsonObject json) {

        mark = json.getAsJsonObject("board").getAsJsonObject("car").get("mark").getAsString();
        model = json.getAsJsonObject("board").getAsJsonObject("car").get("model").getAsString();
        number = json.getAsJsonObject("board").getAsJsonObject("car").get("number").getAsString();
        phoneDriver = json.getAsJsonObject("board").getAsJsonObject("driver").get("phone").getAsString();
        JsonArray arrRoute = json.getAsJsonArray("route");
        latitudeFrom = arrRoute.get(0).getAsJsonObject().get("latitude").getAsDouble();
        longitudeFrom = arrRoute.get(0).getAsJsonObject().get("longitude").getAsDouble();

        latitudeWhere = arrRoute.get(1).getAsJsonObject().get("latitude").getAsDouble();
        longitudeWhere = arrRoute.get(1).getAsJsonObject().get("longitude").getAsDouble();

        amount = json.get("calculatedAmount").getAsDouble();
        status = json.get("status").getAsString();
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPhoneDriver() {
        return phoneDriver;
    }

    public void setPhoneDriver(String phoneDriver) {
        this.phoneDriver = phoneDriver;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getWhere() {
        return where;
    }

    public void setWhere(String where) {
        this.where = where;
    }

    public double getLatitudeFrom() {
        return latitudeFrom;
    }

    public void setLatitudeFrom(double latitudeFrom) {
        this.latitudeFrom = latitudeFrom;
    }

    public double getLongitudeFrom() {
        return longitudeFrom;
    }

    public void setLongitudeFrom(double longitudeFrom) {
        this.longitudeFrom = longitudeFrom;
    }

    public double getLatitudeWhere() {
        return latitudeWhere;
    }

    public void setLatitudeWhere(double latitudeWhere) {
        this.latitudeWhere = latitudeWhere;
    }

    public double getLongitudeWhere() {
        return longitudeWhere;
    }

    public void setLongitudeWhere(double longitudeWhere) {
        this.longitudeWhere = longitudeWhere;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}

package com.max.client.RestService.Interfaces;

import androidx.annotation.NonNull;

import com.google.gson.JsonObject;

public interface GetOrderData {

    void onSuccessCalculate(@NonNull double amount);
    void onErrorCalculate(@NonNull Throwable throwable);

    void onSuccessCreateOrder(@NonNull String orderId);
    void onErrorCreateOrder(@NonNull Throwable throwable);
}

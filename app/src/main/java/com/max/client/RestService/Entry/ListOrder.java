package com.max.client.RestService.Entry;

import com.google.gson.JsonObject;

public class ListOrder {

    private String dataStart;
    private String dataEnd;
    private String from;
    private String where;
    private String mark = null;
    private String model = null;
    private String number = null;
    private String price;
    private String tarif;
    private String phoneDriver;

    String returnRusStatus(String status) {
        if (status.equals("OnWayToClient")) {
            return "В пути к клиенту";
        } else if (status.equals("Waiting")) {
            return "Ожидание";
        } else if (status.equals("OnWay")) {
            return "В пути";
        } else if (status.equals("Completed")) {
            return "Выполнен";
        }
        else if (status.equals("Canceled")) {
            return "Отменен";
        }
        return null;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    private String status;

    public ListOrder(JsonObject json) {

        price = json.get("calculatedAmount").getAsString();
        status = returnRusStatus(json.get("status").getAsString());
        tarif = json.getAsJsonObject("tarif").get("name").getAsString();
        if (json.has("board")) {
            phoneDriver =json.getAsJsonObject("board").getAsJsonObject("driver").get("phone").getAsString();
            mark = json.getAsJsonObject("board").getAsJsonObject("car").get("mark").getAsString();
            model = json.getAsJsonObject("board").getAsJsonObject("car").get("model").getAsString();
            number = json.getAsJsonObject("board").getAsJsonObject("car").get("number").getAsString();
        }
        if(json.has("end"))
        {
            dataEnd = json.get("end").getAsString();
        }
        dataStart = json.get("start").getAsString();

    }

    public String getDataStart() {
        return dataStart;
    }

    public void setDataStart(String dataStart) {
        this.dataStart = dataStart;
    }

    public String getDataEnd() {
        return dataEnd;
    }

    public void setDataEnd(String dataEnd) {
        this.dataEnd = dataEnd;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getWhere() {
        return where;
    }

    public void setWhere(String where) {
        this.where = where;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getTarif() {
        return tarif;
    }

    public void setTarif(String tarif) {
        this.tarif = tarif;
    }

    public String getPhoneDriver() {
        return phoneDriver;
    }

    public void setPhoneDriver(String phoneDriver) {
        this.phoneDriver = phoneDriver;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}

package com.max.client.RestService.Interfaces;

import androidx.annotation.NonNull;

import com.google.gson.JsonObject;


public interface GetValueDataClient {
    void onSuccessInfo(@NonNull JsonObject json);
    void onErrorInfo(@NonNull Throwable throwable);
}

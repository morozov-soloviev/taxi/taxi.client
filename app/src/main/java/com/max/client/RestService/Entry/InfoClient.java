package com.max.client.RestService.Entry;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.List;

public class InfoClient {

    private JsonObject json;

    private String id;
    private String firstName;
    private String lastName;
    private String middleName;
    private String gender;
    private String phone;
    private String email;

    private String orderId;


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    private Tarifs[] tarifs;
    private CarTypes[] carTypes;
    private CarOptions[] carOptions;

    public InfoClient(JsonObject json)
    {
        this.json = json;

        setId(json.getAsJsonObject("client").get("id").getAsString());
        setFirstName(json.getAsJsonObject("client").get("firstName").getAsString());
        setLastName(json.getAsJsonObject("client").get("lastName").getAsString());
        setMiddleName(json.getAsJsonObject("client").get("middleName").getAsString());
        setGender(json.getAsJsonObject("client").get("gender").getAsString());
        setPhone(json.getAsJsonObject("client").get("phone").getAsString());
        setEmail(json.getAsJsonObject("client").get("email").getAsString());

        JsonArray jsonArrayTarifs = json.getAsJsonArray("tarifs");

        tarifs = new Tarifs[jsonArrayTarifs.size()];
        for(int i = 0;i<jsonArrayTarifs.size(); i++)
            tarifs[i] = new Tarifs(jsonArrayTarifs.get(i).getAsJsonObject());

        JsonArray jsonArrayCarTypes = json.getAsJsonArray("carTypes");

        carTypes = new CarTypes[jsonArrayCarTypes.size()];
        for(int i = 0;i<jsonArrayCarTypes.size(); i++)
            carTypes[i] = new CarTypes(jsonArrayCarTypes.get(i).getAsJsonObject());

        JsonArray jsonArrayCarOptions = json.getAsJsonArray("carTypes");

        carOptions = new CarOptions[jsonArrayCarOptions.size()];
        for(int i = 0;i<jsonArrayCarOptions.size(); i++)
            carOptions[i] = new CarOptions(jsonArrayCarOptions.get(i).getAsJsonObject());
    }

    public JsonObject getJson() {
        return json;
    }

    public void setJson(JsonObject json) {
        this.json = json;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public class Tarifs {

        private String id;
        private String name;
        private int rank;

        public Tarifs(JsonObject json)
        {
            setId(json.get("id").getAsString());
            setName(json.get("name").getAsString());
            setRank(json.get("rank").getAsInt());
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getRank() {
            return rank;
        }

        public void setRank(int rank) {
            this.rank = rank;
        }
    }

    public class CarTypes {

        private String id;
        private String name;

        public CarTypes(JsonObject json)
        {
            setId(json.get("id").getAsString());
            setName(json.get("name").getAsString());
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public class CarOptions {

        private String id;
        private String name;

        public CarOptions(JsonObject json)
        {
            setId(json.get("id").getAsString());
            setName(json.get("name").getAsString());
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}

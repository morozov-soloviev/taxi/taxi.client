package com.max.client.RestService.Entry;

public class Order {

    private String tarifId;
    private Routes[] Route;

    private String[] CarTypesIds;
    private String[] CarOptionsIds;
    private String Start;

    public Routes[] getRoute() {
        return Route;
    }

    public void setRoute(Routes[] route) {
        Route = route;
    }

    public String[] getCarTypesIds() {
        return CarTypesIds;
    }

    public void setCarTypesIds(String[] carTypesIds) {
        CarTypesIds = carTypesIds;
    }

    public String[] getCarOptionsIds() {
        return CarOptionsIds;
    }

    public void setCarOptionsIds(String[] carOptionsIds) {
        CarOptionsIds = carOptionsIds;
    }

    public String getStart() {
        return Start;
    }

    public void setStart(String start) {
        Start = start;
    }

    public Order() {
    }

    public String getTarifId() {
        return tarifId;
    }

    public void setTarifId(String tarifId) {
        this.tarifId = tarifId;
    }

}

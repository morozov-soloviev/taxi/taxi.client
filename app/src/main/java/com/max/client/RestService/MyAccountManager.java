package com.max.client.RestService;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;

public class MyAccountManager {

    private String TYPE_ACCOUNT;
    private AccountManager accManager;
    private Account account;

    public MyAccountManager(String Type_account)
    {
        TYPE_ACCOUNT = Type_account;
    }

    public void addAccount(Context context, String username, String password) {
        accManager = AccountManager.get(context);
        account = new Account(username, TYPE_ACCOUNT);
        accManager.addAccountExplicitly(account, password, null);
    }

    public  Account getUserAccount(Context context) {
        accManager = AccountManager.get(context);
        account = null;

        try {
            account = accManager.getAccountsByType(TYPE_ACCOUNT)[0];
        } catch (Exception ignored) {

        }
        return account;
    }

    public String getPassword()
    {
       return accManager.getPassword(account);
    }

    public String getLogin()
    {
        return accManager.getPreviousName(account);
    }
}

package com.max.client.RestService.Interfaces;

import androidx.annotation.NonNull;

import com.google.gson.JsonObject;

public interface GetDataInfoOrder {

    void onSuccessInfoOrder(@NonNull JsonObject json);
    void onErrorInfoOrder(@NonNull Throwable throwable);
}

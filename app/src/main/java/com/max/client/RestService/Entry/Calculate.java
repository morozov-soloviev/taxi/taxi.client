package com.max.client.RestService.Entry;

public class Calculate {

    private String tarifId;
    private Routes[] Route;

    public Calculate() {
    }

    public Calculate(String tarifId, Routes[] Route) {
        this.tarifId = tarifId;
        this.Route = Route;
    }

    public Routes[] getRoute() {
        return Route;
    }

    public void setRoute(Routes[] route) {
        Route = route;
    }

    public String getTarifId() {
        return tarifId;
    }

    public void setTarifId(String tarifId) {
        this.tarifId = tarifId;
    }




}

package com.max.client.RestService.Interfaces;

import com.google.gson.JsonObject;
import com.max.client.RestService.Entry.Calculate;
import com.max.client.RestService.Entry.Client;
import com.max.client.RestService.Entry.LoginClient;
import com.max.client.RestService.Entry.Order;
import com.max.client.RestService.Entry.OrderId;
import com.max.client.RestService.Entry.RegisterClient;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ClientApi {

    @Headers("Content-Type: application/json")
    @POST("client/user/login")
    Call<JsonObject> login(@Body LoginClient loginClient);

    @Headers("Content-Type: application/json")
    @POST("client/user/register")
    Call<JsonObject> register(@Body RegisterClient registerClient);

    @Headers("Content-Type: application/json")
    @POST("client/user/update")
    Call<Void> update(@Body RegisterClient registerClient);

    @GET("client/info")
    Call<JsonObject> info(@Header("Authorization") String token);

    @POST("client/order/calculate")
    Call<JsonObject> calculate(@Header("Authorization") String token, @Body Calculate calculate);

    @POST("client/order/create")
    Call<JsonObject> createOrder(@Header("Authorization") String token, @Body Order order);

    @POST("client/order/info")
    Call<JsonObject> infoOrder(@Header("Authorization") String token, @Body OrderId orderId);

    @GET("client/order/list")
    Call<JsonObject> getListOrder(@Header("Authorization") String token);

    @POST("client/order/cancel")
    Call<JsonObject> cancelOrder(@Header("Authorization") String token,  @Body OrderId orderId);

    @POST("client/user/update")
    Call<JsonObject> updateClient(@Header("Authorization") String token,  @Body Client client);

}

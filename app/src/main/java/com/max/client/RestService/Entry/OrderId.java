package com.max.client.RestService.Entry;

public class OrderId {

    private String OrderId;

    public OrderId(String orderId) {
        OrderId = orderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getOrderId() {
        return OrderId;
    }
}

package com.max.client.RestService.Interfaces;

import androidx.annotation.NonNull;

public interface GetValueURL {
    void onSuccessLogin(@NonNull String token);
    void onErrorLogin(@NonNull String s);

    void onSuccessRegister(@NonNull boolean success);
    void onErrorRegister(@NonNull String s);
}

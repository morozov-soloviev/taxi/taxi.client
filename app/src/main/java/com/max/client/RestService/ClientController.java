package com.max.client.RestService;

import android.util.Log;

import androidx.annotation.Nullable;

import com.google.gson.JsonObject;
import com.max.client.RestService.Entry.Calculate;
import com.max.client.RestService.Entry.Client;
import com.max.client.RestService.Entry.LoginClient;
import com.max.client.RestService.Entry.Order;
import com.max.client.RestService.Entry.OrderId;
import com.max.client.RestService.Entry.RegisterClient;
import com.max.client.RestService.Interfaces.CancelInterface;
import com.max.client.RestService.Interfaces.ClientApi;
import com.max.client.RestService.Interfaces.GetDataInfoOrder;
import com.max.client.RestService.Interfaces.GetOrderData;
import com.max.client.RestService.Interfaces.GetValueDataClient;
import com.max.client.RestService.Interfaces.GetValueURL;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClientController {

    private ClientApi clientApi;

    public ClientController(String login) {
        RetrofitBuild.start();
        clientApi = RetrofitBuild.retrofit.create(ClientApi.class);
    }

    public ClientController() {
        clientApi = RetrofitBuild.retrofit.create(ClientApi.class);
    }


    public void register(RegisterClient rClient, @Nullable final GetValueURL callbacks) {
        clientApi.register(rClient).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if(!response.body().has("error")) {
                    if (callbacks != null)
                        callbacks.onSuccessRegister(response.body().get("success").getAsBoolean());
                }
                else
                {
                    if(response.body().get("error").getAsString().equals("user_already_exist")) {
                        if (callbacks != null)
                            callbacks.onErrorRegister("Пользователь с таким логином уже существует");
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (callbacks != null)
                    callbacks.onErrorRegister(t.getMessage());
            }
        });
    }

    public void update(@Nullable final GetValueURL callbacks, Client client) {
        clientApi.updateClient(RetrofitBuild.token, client).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (callbacks != null) {
                    if (response.isSuccessful()) {
                        callbacks.onSuccessRegister(response.body().get("success").getAsBoolean());
                    } else {
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (callbacks != null)
                    callbacks.onErrorRegister(t.getMessage());
            }
        });
    }


    public void login(LoginClient lc, @Nullable final GetValueURL callbacks) {
        clientApi.login(lc).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                String token = null;
                if(response.body().has("data")) {
                    token = response.body().getAsJsonObject("data").get("token").getAsString();
                    if (callbacks != null)
                        callbacks.onSuccessLogin(token);
                }
                else{
                    if(response.body().get("error").getAsString().equals("wrong_login_or_password")) {
                        if (callbacks != null)
                            callbacks.onErrorLogin("Логин или пароль неверный");
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (callbacks != null)
                    callbacks.onErrorLogin(t.getMessage());
            }
        });
    }



    public void info(@Nullable final GetValueDataClient callbacks) {
        clientApi.info(RetrofitBuild.token).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (callbacks != null) {
                    if (response.isSuccessful()) {
                        callbacks.onSuccessInfo(response.body().getAsJsonObject("data"));
                        Log.d("INFOOO", response.body().toString());
                    } else {
                        Log.d("INFOOO", response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (callbacks != null)
                    callbacks.onErrorInfo(t);
                Log.d("INFOOO", t.getMessage());
            }
        });
    }

    public void calculate(Calculate calculate, @Nullable final GetOrderData callbacks) {
        clientApi.calculate(RetrofitBuild.token, calculate).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (callbacks != null) {
                    if (response.isSuccessful()) {
                        callbacks.onSuccessCalculate(response.body().getAsJsonObject("data").get("amount").getAsDouble());
                        Log.d("calc", response.body().toString());
                    } else {
                        Log.d("calc", response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (callbacks != null)
                    callbacks.onErrorCalculate(t);
                Log.d("calc", t.getMessage());
            }
        });
    }

    public void createOrder(Order order, @Nullable final GetOrderData callbacks) {
        clientApi.createOrder(RetrofitBuild.token, order).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (callbacks != null) {
                    if (response.isSuccessful()) {
                        callbacks.onSuccessCreateOrder(response.body().getAsJsonObject("data").get("orderId").getAsString());
                        Log.d("order", response.body().toString());
                    } else {
                        Log.d("order", response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (callbacks != null)
                    callbacks.onErrorCreateOrder(t);
                Log.d("order", t.getMessage());
            }
        });
    }

    public void infoOrder(@Nullable final GetDataInfoOrder callbacks, OrderId orderId) {
        clientApi.infoOrder(RetrofitBuild.token, orderId).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (callbacks != null) {
                    if (response.isSuccessful()) {
                        callbacks.onSuccessInfoOrder(response.body().getAsJsonObject("data").getAsJsonObject("order"));
                    } else {
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (callbacks != null)
                    callbacks.onErrorInfoOrder(t);
            }
        });
    }

    public void getListOrder(@Nullable final GetDataInfoOrder callbacks) {
        clientApi.getListOrder(RetrofitBuild.token).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (callbacks != null) {
                    if (response.isSuccessful()) {
                        callbacks.onSuccessInfoOrder(response.body().getAsJsonObject("data"));
                    } else {
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (callbacks != null)
                    callbacks.onErrorInfoOrder(t);
            }
        });
    }
    public void cancelOrder(@Nullable final CancelInterface callbacks, OrderId orderId) {
        clientApi.cancelOrder(RetrofitBuild.token, orderId).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (callbacks != null) {
                    if (response.isSuccessful()) {
                        callbacks.onSuccessCancel(response.body().get("success").getAsBoolean());
                    } else {
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (callbacks != null)
                    callbacks.onErrorCancel(t);
            }
        });
    }
}

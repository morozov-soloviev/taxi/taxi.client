package com.max.client;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;
import com.google.gson.JsonObject;
import com.max.client.RestService.ClientController;
import com.max.client.RestService.Entry.InfoClient;
import com.max.client.RestService.Interfaces.GetValueDataClient;
import com.yandex.mapkit.Animation;
import com.yandex.mapkit.MapKitFactory;
import com.yandex.mapkit.geometry.Point;
import com.yandex.mapkit.map.CameraPosition;
import com.yandex.mapkit.mapview.MapView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity{

    private AppBarConfiguration mAppBarConfiguration;

    private String ApiKey = "6cec9a45-a6a1-48d3-8884-340f9a2c0fb0";

    public static String Login, Password;

    private ClientController client;
    private InfoClient infoClient;
    private ActionBarDrawerToggle mDrawerToggle;

    private TextView nameTxt, phoneTxt, emailTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.order_taxi, R.id.history_order, R.id.account_page)
                .setDrawerLayout(drawer)
                .build();

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        View headerView = navigationView.getHeaderView(0);

        Login = (String)getIntent().getExtras().get("login");
        Password = (String)getIntent().getExtras().get("password");

        client=new ClientController();
        phoneTxt = (TextView) headerView.findViewById(R.id.phoneTxt);
        nameTxt = (TextView) headerView.findViewById(R.id.NameTxt);
        emailTxt = (TextView) headerView.findViewById(R.id.emailTxt);

        client.info(new GetValueDataClient() {
            @Override
            public void onSuccessInfo(@NonNull JsonObject json) {
                infoClient = new InfoClient(json);
                phoneTxt.setText(infoClient.getPhone());
                nameTxt.setText(infoClient.getFirstName() + " " + infoClient.getLastName());
                emailTxt.setText(infoClient.getEmail());
            }

            @Override
            public void onErrorInfo(@NonNull Throwable throwable) {

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
}

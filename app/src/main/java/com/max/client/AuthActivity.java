package com.max.client;

import android.accounts.Account;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.max.client.RestService.ClientController;
import com.max.client.RestService.Interfaces.GetValueURL;
import com.max.client.RestService.Entry.LoginClient;
import com.max.client.RestService.MyAccountManager;
import com.max.client.RestService.Entry.RegisterClient;
import com.max.client.RestService.RetrofitBuild;


public class AuthActivity extends AppCompatActivity implements GetValueURL {

    private Button btnRegister, btnLogIn, btnApplyRegister, btnSignIn, btnBackRegister, btnBackSign;
    private EditText inpLogin, inpPassword, inpEmail, inpPhone, inpName, inpSurname, login, password;
    private ScrollView scrollRegister, scrollSignIn;
    private TextView banner, nameAppTxt, textLoad;

    private ClientController client;

    private static final String TYPE_ACCOUNT = "com.taxi.su";
    private MyAccountManager managerAcc;
    private Account account;
    private ProgressBar progressBarLoad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.auth_activity);

        client = new ClientController("login");
        textLoad = (TextView)findViewById(R.id.textLoad);
        progressBarLoad = (ProgressBar)findViewById(R.id.progressLoad);
        scrollSignIn = (ScrollView)findViewById(R.id.scrollSignIn);
        btnBackSign = (Button) findViewById(R.id.btnBackSign);
        scrollRegister = (ScrollView)findViewById(R.id.scrollRegister);
        btnBackRegister = (Button)findViewById(R.id.btnBackReg);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnLogIn = (Button) findViewById(R.id.btnLogIn);
        btnApplyRegister = (Button) findViewById(R.id.btnApplyRegister);
        banner = (TextView) findViewById(R.id.Banner);
        nameAppTxt = (TextView) findViewById(R.id.nameAppTxt);

        btnSignIn = (Button) findViewById(R.id.btn_sign_in);
        login = (EditText) findViewById(R.id.login);
        password = (EditText) findViewById(R.id.password);

        inpLogin = (EditText) findViewById(R.id.inpLogin);
        inpPassword = (EditText) findViewById(R.id.inpPassword);
        inpEmail = (EditText) findViewById(R.id.inpEmail);
        inpPhone = (EditText) findViewById(R.id.inpPhone);
        inpName = (EditText) findViewById(R.id.inpName);
        inpSurname = (EditText) findViewById(R.id.inpSurname);

        btnBackRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                scrollRegister.setVisibility(View.INVISIBLE);

                btnRegister.setVisibility(View.VISIBLE);
                btnLogIn.setVisibility(View.VISIBLE);
                banner.setVisibility(View.VISIBLE);
                nameAppTxt.setVisibility(View.VISIBLE);
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                scrollRegister.setVisibility(View.VISIBLE);

                btnRegister.setVisibility(View.INVISIBLE);
                btnLogIn.setVisibility(View.INVISIBLE);
                banner.setVisibility(View.INVISIBLE);
                nameAppTxt.setVisibility(View.INVISIBLE);
            }
        });

        btnApplyRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                register();

            }
        });

        btnLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollSignIn.setVisibility(View.VISIBLE);

                btnRegister.setVisibility(View.INVISIBLE);
                btnLogIn.setVisibility(View.INVISIBLE);
                banner.setVisibility(View.INVISIBLE);
                nameAppTxt.setVisibility(View.INVISIBLE);
            }
        });

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                login();

            }
        });

        btnBackSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollSignIn.setVisibility(View.INVISIBLE);

                btnRegister.setVisibility(View.VISIBLE);
                btnLogIn.setVisibility(View.VISIBLE);
                banner.setVisibility(View.VISIBLE);
                nameAppTxt.setVisibility(View.VISIBLE);
            }
        });
    }

    public void register() {
        RegisterClient rc = new RegisterClient();
        rc.setLogin(inpLogin.getText().toString());
        rc.setPassword(inpPassword.getText().toString());
        rc.setEmail(inpEmail.getText().toString());
        rc.setPhone(inpPhone.getText().toString());
        rc.setFirstName(inpName.getText().toString());
        rc.setLastName(inpSurname.getText().toString());
        scrollRegister.setVisibility(View.INVISIBLE);
        progressBarLoad.setVisibility(View.VISIBLE);
        textLoad.setText("   Регистрация...");
        textLoad.setVisibility(View.VISIBLE);
        client.register(rc, this);
    }

    public void login() {
        LoginClient lc = new LoginClient();
        lc.setLogin(login.getText().toString());
        lc.setPassword(password.getText().toString());
        scrollSignIn.setVisibility(View.INVISIBLE);
        progressBarLoad.setVisibility(View.VISIBLE);
        textLoad.setText("   Вход...");
        textLoad.setVisibility(View.VISIBLE);
        client.login(lc, this);
    }

    @Override
    public void onSuccessLogin(@NonNull String token) {
        if (token != null) {
            RetrofitBuild.token = "Bearer " + token;

            Intent intent = new Intent(AuthActivity.this, MainActivity.class);
            intent.putExtra("login", login.getText().toString());
            intent.putExtra("password", password.getText().toString());
            startActivity(intent);
        }
    }

    @Override
    public void onErrorLogin(@NonNull String s) {
        scrollSignIn.setVisibility(View.VISIBLE);
        progressBarLoad.setVisibility(View.INVISIBLE);
        textLoad.setVisibility(View.INVISIBLE);
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccessRegister(@NonNull boolean success) {
        if (success = true) {
            //managerAcc.addAccount(this, inpLogin.getText().toString(), inpPassword.getText().toString());
            Toast.makeText(this, "Пользователь зарегестрирован!", Toast.LENGTH_SHORT).show();
            progressBarLoad.setVisibility(View.INVISIBLE);
            textLoad.setVisibility(View.INVISIBLE);
            scrollRegister.setVisibility(View.INVISIBLE);

            btnRegister.setVisibility(View.VISIBLE);
            btnLogIn.setVisibility(View.VISIBLE);
            banner.setVisibility(View.VISIBLE);
            nameAppTxt.setVisibility(View.VISIBLE);
        }
        else{
            Toast.makeText(this, "Такой пользователь уже существует!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onErrorRegister(@NonNull String s) {
        scrollRegister.setVisibility(View.VISIBLE);
        progressBarLoad.setVisibility(View.INVISIBLE);
        textLoad.setVisibility(View.INVISIBLE);
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }
}

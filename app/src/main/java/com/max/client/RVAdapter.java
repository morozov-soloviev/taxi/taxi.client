package com.max.client;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.max.client.RestService.Entry.ListOrder;

import java.util.ArrayList;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.PersonViewHolder> {


    ArrayList<ListOrder> listOrders;

    public RVAdapter(ArrayList<ListOrder> listOrders) {

        this.listOrders = listOrders;
    }

    @NonNull
    @Override
    public PersonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.order, parent, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }


    @Override
    public void onBindViewHolder(@NonNull PersonViewHolder holder, int position) {
        holder.bind(listOrders.get(position));
    }


    @Override
    public int getItemCount() {
        return listOrders.size();
    }


    public static class PersonViewHolder extends RecyclerView.ViewHolder {
        TextView from;
        TextView where;
        TextView price, txtNameOrder, status, phone, carTxt;

        PersonViewHolder(View itemView) {
            super(itemView);
            from = (TextView) itemView.findViewById(R.id.fromHistory);
            where = (TextView) itemView.findViewById(R.id.whereHistory);
            price = (TextView) itemView.findViewById(R.id.PriceHistory);
            txtNameOrder = (TextView) itemView.findViewById(R.id.NameOrderHistory);
            status = (TextView) itemView.findViewById(R.id.StatusHistory);
            phone = (TextView) itemView.findViewById(R.id.PhoneHistory);
            carTxt = (TextView) itemView.findViewById(R.id.CarHistory);
        }

        public void bind(ListOrder listOrder) {

            phone.setText(listOrder.getPhoneDriver());
            from.setText(listOrder.getFrom());
            where.setText(listOrder.getWhere());
            price.setText(listOrder.getPrice() + " р. (" + listOrder.getTarif() + ")");
            carTxt.setText(listOrder.getMark() + " " + listOrder.getModel() + " - " + listOrder.getNumber() + " (" + listOrder.getTarif() + ")");
            if (listOrder.getDataEnd() != null)
                status.setText(listOrder.getStatus() + " (" + listOrder.getDataEnd() + ")");
            else
                status.setText(listOrder.getStatus());
            txtNameOrder.setText("Заказ (" + listOrder.getDataStart() + ")");
        }


    }
}
